from django.apps import AppConfig


class CoordenadorCursoConfig(AppConfig):
    name = 'coordenador_curso'
